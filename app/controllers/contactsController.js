// Dependencies
var mongoose = require('mongoose');
var Contact = require('../models/contact.js');

//GET - Return all registers
exports.findAll = function(req, res) {
 Contact.find(function(err, contacts) {
 if(err) return res.status(500).send(err.message);
 res.status(200).json(contacts);
 });
};

//GET - Return a register with specified ID
exports.findById = function(req, res) {
 Contact.findById(req.params.id, function(err, contact) {
 if(err) return res.status(500).send(err.message);
 res.status(200).json(contact);
 });
};

//POST - Insert a new register
exports.add = function(req, res) {
 var contact = new Contact({
 name: req.body.name,
 surname: req.body.surname,
 phone: req.body.phone,
 email: req.body.email,
 address: req.body.address,
 birthday: req.body.birthday
 });
 
 contact.save(function(err, contact) {
 if(err) return res.status(500).send(err.message);
 res.status(200).json(contact);
 });
};

//PUT - Update a register already exists
exports.update = function(req, res) {
 Contact.findById(req.params.id, function(err, contact) {
 	contact.name 	 = req.body.name	 || contact.name;
 	contact.surname  = req.body.surname  || contact.name;
 	contact.phone	 = req.body.phone	 || contact.phone;
 	contact.email	 = req.body.email	 || contact.email;
 	contact.address	 = req.body.address	 || contact.address;
 	contact.birthday = req.body.birthday || contact.birthday;
 contact.save(function(err) {
 if(err) return res.status(500).send(err.message);
 res.status(200).json(contact);
 });
 });
};

//DELETE - Delete a register with specified ID
exports.delete = function(req, res) {
 Contact.findById(req.params.id, function(err, contact) {
 contact.remove(function(err) {
 if(err) return res.status(500).send(err.message);
 res.status(200).send();
 });
 });
};