// Dependencies
var ContactCtrl = require('../controllers/contactsController.js');

// Opens App Routes
module.exports = function(express,app) {

	//HOME
	app.get('/', function(req, res, next) {
	 res.sendfile('index.html');
	});

	//API
	var api = express.Router();
	//Clients
	api.route('/contacts') 
	.get(ContactCtrl.findAll)
	.post(ContactCtrl.add);
	api.route('/contacts/:id') 
	.get(ContactCtrl.findById)
	.put(ContactCtrl.update)
	.delete(ContactCtrl.delete);
	app.use('/api/agenda/', api);

};